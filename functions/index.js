const functions = require("firebase-functions");
const app = require("express")();
const { db } = require("./util/admin");
const { ROLE } = require("./util/data");
const {
  authUser,
  authRole,
  setUserProfile,
  authUploadImage,
  authEditDetails,
  authViewProfile,
} = require("./util/fbAuth");

const {
  getAllUsers,
  registerUser,
  loginUser,
  uploadImage,
  editUserDetails,
  getDriverStatusLog,
  getUser,
  setUserLog,
  viewUserLog,
  getDriverCommission,
  getCustomerList,
  calculateDriverSalary
} = require("./handlers/users");

const {
  setProfile,
  locationOnTrip,
  getDriverLocation,
  getDriverAlcoholStatus,
  postATrip,
  startTour,
  endtTour,
  getTripStatus,
  postDriverCommision,
} = require("./handlers/drivers");

//User Routes
app.get("/users", authUser, getAllUsers);
app.get("/user/:handle", authUser, getUser);
app.get("/userlog/:handle", authUser, authRole(ROLE.ADMIN), viewUserLog);

app.post("/registeruser", authUser, authRole(ROLE.ADMIN), registerUser);
app.post("/login", loginUser);
app.post(
  "/user/:handle/image",
  setUserProfile,
  authUser,
  authUploadImage,
  uploadImage
);
app.post(
  "/user/:handle",
  setUserProfile,
  authUser,
  authEditDetails,
  editUserDetails
);
app.post("/userlog/:handle", authUser, authRole(ROLE.ADMIN), setUserLog);

//**Special Routes**
//Driver Routes
app.get("/welcome", authUser, authRole(ROLE.DRIVER), setProfile);
app.get("/drivers/:rfid", authUser, authViewProfile, getDriverStatusLog);
app.get(
  "/driver/:handle/commission",
  authUser,
  authRole(ROLE.ADMIN),
  getDriverCommission
);
app.get(
  "/driver/:rfid/location",
  authUser,
  authRole(ROLE.ADMIN),
  getDriverLocation
);
app.get("/customers", authUser, getCustomerList);
app.get("/tripstatus/:tripId", authUser, authRole(ROLE.DRIVER), getTripStatus);
app.post("/driver/:handle/salary", authUser, authRole(ROLE.ADMIN), calculateDriverSalary);
app.post(
  "/driver/:handle/alcohol",
  authUser,
  authViewProfile,
  getDriverAlcoholStatus
);
app.post("/driver/location", authUser, authRole(ROLE.DRIVER), locationOnTrip); // Update location
app.post("/trip", authUser, authRole(ROLE.DRIVER), postATrip);
app.post("/start/:tripId", authUser, authRole(ROLE.DRIVER), startTour);
app.post("/end/:tripId", authUser, authRole(ROLE.DRIVER), endtTour);
app.post(
  "driver/:tripId",
  authUser,
  authRole(ROLE.DRIVER),
  postDriverCommision
);

exports.api = functions.region("us-central1").https.onRequest(app);

//Alcohol change status log
exports.onAlcoholChange = functions
  .region("us-central1")
  .firestore.document("/driverlog/{rfid}")
  .onUpdate((change) => {
    if (
      change.before.data().drunken !== change.after.data().drunken &&
      change.after.data().drunken === true
    ) {
      const alcoholStatus = {
        createdAt: new Date().toISOString(),
        drunken: true,
        handle: change.before.data().handle,
        tripId: change.before.data().tripId,
        month: new Date().toLocaleString("en-us", { month: "long" }),
        year: new Date().getFullYear(),
      };

      let rfid = change.before.data().rfid;

      return db
        .collection("alcohol")
        .add(alcoholStatus)
        .then(() => {
          return db.doc(`/driverlog/${rfid}`).update({
            status: "drunken",
          });
        }).then(()=>{
          return db.doc(`/trip/${alcoholStatus.tripId}`).update({
            drunken: true,
          });
        })
        .catch((err) => {
          console.error(err);
        });
    }else{
      return true;
    }
  });
